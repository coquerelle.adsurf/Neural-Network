﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using System;
using System.Reflection;


public class Manager : MonoBehaviour {



	public GameObject catPrefab;            //Prefab de notre agent

	private bool isTraning = false;
	public int populationSize = 50;			//Nombre d'agent a faire spawner. Gardez un multiple de 4 svp
	public float timer = 15f;				//Temps par generation			
	private int generationNumber = 0;       //Numero de la generation actuelle
	private int targetHit = 0;
	
	private List<NeuralNetwork> nets;		//Liste de neural networks de generation x
	private List<NeuralNetwork> newNets;    //Liste de neural networks de generation x+1
	private List<NeuralNetwork> saveNets;    //Liste de neural networks à sauvegerder
	public List<GameObject> catList = null; //Liste des chats de notre generation


	private int[] layers = new int[] { 8, 5, 8, 4, 2 }; //Dimension de nos reseaux de neurones
	
	private float fit=0;					//On calculera la moyenne de fitnes de la generation grace a cette variable
	
	// Met fin a la generation actuelle (cheh)
	void Timer()
	{
		isTraning = false;
	}


	void Update ()
	{
		//Changement de generation
		if (isTraning == false)
		{
			// Si c'est la premiere generation, instancie les agents
			if (generationNumber == 0)
			{
				GameObject.Find("Agent").SetActive(false);
				if (File.Exists(Application.dataPath + "/Weights/weight.json"))
				{
					string jsonEntries = File.ReadAllText(Application.dataPath + "/Weights/weight.json");
					SerializableArrayNN ANN = JsonConvert.DeserializeObject<SerializableArrayNN>(jsonEntries);
					List<SerializableNeuralNetwork> arrNN  =  ANN.arrOfNNs;
					generationNumber = ANN.generationNumber;
					Debug.Log("Generation Number:" + generationNumber);
					InitCatNeuralNetworksWithSave(arrNN);
				} 
				else
                {
					InitCatNeuralNetworks();
				}

				CreateCatBodies();
			}
			else
			{
				Debug.Log("Generation Number:" + generationNumber);
				//Transfer le score de fitness du controleur vers le reseau de neurones
				for (int i = 0; i < populationSize; i++)
				{
					NNController script = catList[i].GetComponent<NNController>();
					float fitness = script.fitness;
					nets[i].SetFitness(fitness);
				}

				//Trie les agents pour ne garder que les plus performants
				nets.Sort();
				nets.Reverse();

				//sauvegarder les agents
				List<SerializableNeuralNetwork> arrOfNNs = new List<SerializableNeuralNetwork>();
				string finalJson = "";
				for (int i = 0; i < populationSize; i++)
				{
					NeuralNetwork net = new NeuralNetwork(nets[i]);
					SerializableNeuralNetwork SNN = new SerializableNeuralNetwork(net.weights, net.fitness);
					arrOfNNs.Add(SNN);
				}
				SerializableArrayNN ANN = new SerializableArrayNN(arrOfNNs, generationNumber);
				finalJson = JsonConvert.SerializeObject(ANN);
				File.WriteAllText(Application.dataPath + "/Weights/weight.json", finalJson);

				//Affiche la moyenne de fitness de la generation
				fit = 0;
				for (int i = 0; i < populationSize; i++)
				{
					fit += nets[i].GetFitness();
				}
				fit /= populationSize;
				Debug.Log("Average fitness:");
				Debug.Log(fit);
				Debug.Log("Target was hit: " + targetHit.ToString());
				targetHit = 0;


				//Instancie la liste de la generation suivante
				List<NeuralNetwork> newNets = new List<NeuralNetwork>();

				//Recupere les plus intelligent de nos agents
				for (int i = 0; i < populationSize/4; i++)
				{
					NeuralNetwork net = new NeuralNetwork(nets[i]);
					newNets.Add (net);
				}

				//Recupere les plus intelligent de nos agents et les fait  muter
				for (int i = 0; i < populationSize/4; i++)
				{
					NeuralNetwork net = new NeuralNetwork(nets[i]);
					net.Mutate(0.5f);
					newNets.Add (net);
				}

				//Recupere les plus intelligent de nos agents et les fait plus muter
				for (int i = 0; i < populationSize/4; i++)
				{
					NeuralNetwork net = new NeuralNetwork(nets[i]);
					net.Mutate(2f);
					newNets.Add (net);

				}

				//Recupere les plus intelligent de nos agents et leurs defonce le cerveau
				for (int i = 0; i < populationSize/4; i++)
				{
					NeuralNetwork net = new NeuralNetwork(nets[i]);
					net.Mutate(10f);
					newNets.Add (net);
				}				

				//Changement d'agents entre les deux generation
				nets = newNets;

			}
			
			//A la fin du decompte du timer, passe a la generation suivante
			generationNumber++;
			Invoke("Timer",timer);
			CreateCatBodies();
			
			
			isTraning = true;
		}

		//------------------FEEDFORWARD
		//Transfer les informations du NNController vers l'input layer du reseau de neurones
		for (int i = 0; i < populationSize; i++)
		{
			NNController script = catList[i].GetComponent<NNController>();

			if (script.targetHit) {
				targetHit++;
				script.targetHit = false;
			}

			float[] result;
			float vel = script.currentVelocity;
			float distForward = script.distForward/script.maxDistance;
			float distLeft = script.distLeft/script.maxDistance;
			float distRight = script.distRight/script.maxDistance;
			float distDiagLeft = script.distDiagLeft/script.maxDistance;
			float distDiagRight = script.distDiagRight/script.maxDistance;
			float angleForwardTarget = (float)script.angle;
			float deltaFitness = script.deltaFitness;

			float[] tInputs = new float[]{vel, distForward, distLeft, distRight, distDiagLeft, distDiagRight, angleForwardTarget, deltaFitness };
			result = nets[i].FeedForward(tInputs);
			script.results = result;//Envoie le resultat au controleur

		}

        //Les generation ne meurent pas assez vite pour vous? Voila la solution
		if (Input.GetKeyDown("space")) 
		{
			generationNumber++;
			isTraning = true;
			Timer ();
			CreateCatBodies();
			
		}
	}

	//Instancie tout nos agents
	private void CreateCatBodies()
	{
		//Detruit tout nos agents (muahaha)
		for (int i = 0; i < catList.Count; i++)
		{
			Destroy(catList[i]);
		}

		//Recree une generation de agents (mooooooh)
		catList = new List<GameObject>();
		for (int i = 0; i < populationSize; i++)
		{
			GameObject cat = Instantiate(catPrefab, new Vector3(-17f, 0, 0),catPrefab.transform.rotation);
			cat.SetActive(true);
			catList.Add(cat);
			catList[i] = cat;
		}
	}	

	// Initialise notre liste d'agent
	void InitCatNeuralNetworks()
	{
		nets = new List<NeuralNetwork>();
		
		for (int i = 0; i < populationSize; i++)
		{
			NeuralNetwork net = new NeuralNetwork(layers);
			net.Mutate(0.5f);
			nets.Add(net);
		}
	}

	// Initialise notre liste d'agent avec les poids sauvegardés
	void InitCatNeuralNetworksWithSave(List<SerializableNeuralNetwork> PatternArrNN)
	{
		nets = new List<NeuralNetwork>();

		for (int i = 0; i < populationSize; i++)
		{
			NeuralNetwork net = new NeuralNetwork(layers);
			SerializableNeuralNetwork obj = PatternArrNN[i];
			//
			net.CopyWeights(obj.weights);
			//
			nets.Add(net);
		}
	}

}

[System.Serializable]
public class SerializableArrayNN
{
	public List<SerializableNeuralNetwork> arrOfNNs;
	public int generationNumber;

	public SerializableArrayNN(List<SerializableNeuralNetwork> PatternArrOfNNs, int PatternGenerationNumber)
	{
		arrOfNNs = PatternArrOfNNs;
		generationNumber = PatternGenerationNumber;
	}
}

[System.Serializable]
public class SerializableNeuralNetwork
{
	public float[][][] weights;//matrice des poids
	public float fitness;       //fitness du reseau

	public SerializableNeuralNetwork(float[][][] patternWeights, float patternFitness)
	{
		weights = patternWeights;
		fitness = patternFitness;
	}
}