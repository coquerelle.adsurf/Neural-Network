﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NNController : MonoBehaviour
{
	//Parametres propre a chaque agent/au joueur
	private Transform transf;
	public Transform other;

	private Vector3 startAt;

	private Vector3 movDir = Vector3.zero;  //Vecteur de deplacement
	public float rotSpeed = 300f;          //Vitesse de rotation
	public float speed = 1f;             //Facteur de vitesse		
	public float initialVelocity = 0.0f;   //Velocite initiale
	public float finalVelocity = 3f;   //Velocite finale
	public float currentVelocity = 0.0f;   //Velocite actuelle
	public float accelerationRate = 2f;    //Vitesse d'acceleration
	public float decelerationRate = 1.6f;  //Vitesse de deceleration

	//Gere chacuns des rayons de detection
	public float CheckPointScore = 5f;
	public float maxDistance = 30f;
	public float distForward = 0f;
	public float distLeft = 0f;
	public float distRight = 0f;
	public float distDiagLeft = 0f;
	public float distDiagRight = 0f;
	public float distBarrier = 0f;

	//Gere le score de fitness
	public float fitness = 0f;
	private float lastFitness = 0f;
	public float deltaFitness = 0f;
	private Vector3 lastPosition;   //Permet le calcul de la distance parcourue
	public float distanceTraveled;

	public bool targetHit = false;
	
	//gerer la distance entre la cible et l'agent.
	public float distOther;
	public float distOtherAtStart;
	public float soClose;
	public double angle = 0d;

	public float[] results;             // Resultat du feed forward. Contient deux float, le premier pour l'acceleration, le deuxieme pour l'orientation
	public bool active = true;          //Passe a False si l'agent heurte un mur

	public Rigidbody rb; // fixer le script à l'objet à controller

	// Start is called before the first frame update
	void Start()
	{
		rb = GetComponent<Rigidbody>();
		startAt = transform.position;
		lastPosition = transform.position;
		distOtherAtStart = Vector3.Distance(other.position, transform.position);
		angle = 0d;
	}

	// Update is called once per frame
	void Update()
	{
		//Tant que l'agent est en vie
		if (active)
		{
			// Si le neural network nous a dit quoi faire... Bah l'agent le fait
			if (results.Length != 0)
			{
				transform.Rotate(0, results[1] * rotSpeed * Time.deltaTime, 0);

				if (results[0] > 0f)
				{
					currentVelocity = currentVelocity + (accelerationRate * Time.deltaTime);
				}
				else
				{
					currentVelocity = currentVelocity - (decelerationRate * Time.deltaTime);
				}

				currentVelocity = Mathf.Clamp(currentVelocity, initialVelocity, finalVelocity);// Clamp la velocite entre la valeur initiale et finale

				transform.Translate(Vector3.forward * currentVelocity * speed * Time.deltaTime);
			}
		}
		
		//calcul distance avec la cible
		distOther = Vector3.Distance(other.position, transform.position);
		soClose = distOther/distOtherAtStart;
		angle = 0d;

		InteractRaycast();// Cree les rayons


		//Gestion du fitness
		distanceTraveled += Vector3.Distance(transform.position, lastPosition);
		lastPosition = transform.position;
		fitness += distanceTraveled / 10000 + 1 / soClose;    //Augmente le fitness en fonction de la distance parcourue et la distance de la cible.
		fitness -= 0.1f;                   //Decroit le fitness au long du temps
		deltaFitness = fitness - lastFitness;
		lastFitness = fitness;

		if (transform.position.y <= -50)
		{
			transform.position = startAt;
			transform.rotation = new Quaternion(0, 0, 0, 0);

		}
	}

	//Se declenche en entrant en collision avec un mur
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Murs")
		{
			active = false;
			fitness /= 5;
		}
		else if (other.gameObject.tag == "CheckPoint")
		{
			fitness *= CheckPointScore;
		}
		else if (other.gameObject.name == "Target")
		{
			fitness *= 10000;
			active = false;
			targetHit = true;
		}

	}

	//Gere les rayons pour detecter les murs
	void InteractRaycast()
	{
		transf = GetComponent<Transform>();
		Vector3 playerPosition = transform.position;

		//Cree les directions de chaque rayons
		Vector3 forwardDirection = transf.forward * 10;
		Vector3 leftDirection = transf.right * -10;
		Vector3 rightDirection = transf.right * 10;
		Vector3 diagLeft = transf.TransformDirection(new Vector3(maxDistance / 5, 0f, maxDistance / 5));
		Vector3 diagRight = transf.TransformDirection(new Vector3(-maxDistance / 5, 0f, maxDistance / 5));
		Vector3 targetDirection = other.position - playerPosition;

		//Cree les rayons
		Ray myRay = new Ray(playerPosition, forwardDirection);
		Ray leftRay = new Ray(playerPosition, leftDirection);
		Ray rightRay = new Ray(playerPosition, rightDirection);
		Ray diagLeftRay = new Ray(playerPosition, diagLeft);
		Ray diagRightRay = new Ray(playerPosition, diagRight);
		Ray barrier = new Ray(playerPosition, targetDirection);

		//Gere les collisions des rayons et retourne la distance si ils rencontrent quelque chose
		RaycastHit hit;
		if (Physics.Raycast(myRay, out hit, maxDistance * 10) && hit.transform.tag == "Murs")
		{
			distForward = hit.distance;
		}
		if (Physics.Raycast(leftRay, out hit, maxDistance * 10))
		{
			distLeft = hit.distance;
		}
		if (Physics.Raycast(rightRay, out hit, maxDistance * 10))
		{
			distRight = hit.distance;
		}
		if (Physics.Raycast(diagLeftRay, out hit, maxDistance * 10))
		{
			distDiagLeft = hit.distance;
		}
		if (Physics.Raycast(diagRightRay, out hit, maxDistance * 10))
		{
			distDiagRight = hit.distance;
		}
		if (Physics.Raycast(barrier, out hit, maxDistance * 10))
		{
            if (hit.transform.name != "Agent")
            {
				if (hit.distance + 2 > distOther)
				{
					angle = Vector3.SignedAngle(transform.forward, other.position - transform.position, Vector3.up);
					fitness += 1 / soClose;
					Debug.DrawRay(transform.position, targetDirection * maxDistance, Color.red);
				}
				else
				{
					//Debug.DrawRay(transform.position, targetDirection * maxDistance, Color.green);
				}
			}

		}

		//Affiche les rayons
		/*Debug.DrawRay(transform.position, forwardDirection * maxDistance, Color.green);
		Debug.DrawRay(transform.position, leftDirection * maxDistance, Color.green);
		Debug.DrawRay(transform.position, rightDirection * maxDistance, Color.green);
		Debug.DrawRay(transform.position, diagLeft * maxDistance, Color.green);
		Debug.DrawRay(transform.position, diagRight * maxDistance, Color.green);*/
		
	}


}
